<?php
// Register Style
function custom_styles() {

	wp_register_style( 'UikitCss', 'https://cdn.jsdelivr.net/npm/uikit@3.7.6/dist/css/uikit.min.css', false, '3.7.6' );
	wp_enqueue_style( 'UikitCss' );

}
add_action( 'wp_enqueue_scripts', 'custom_styles' );

// Register Script
function custom_scripts() {

	wp_register_script( 'UikitJS', 'https://cdn.jsdelivr.net/npm/uikit@3.7.6/dist/js/uikit.min.js', false, '3.7.6', false );
	wp_enqueue_script( 'UikitJS' );

	wp_register_script( 'UikitIcon', 'https://cdn.jsdelivr.net/npm/uikit@3.7.6/dist/js/uikit-icons.min.js', false, '3.7.6', false );
	wp_enqueue_script( 'UikitIcon' );

}
add_action( 'wp_enqueue_scripts', 'custom_scripts' );

// Register Style
function IconsStyle() {

	wp_register_style( 'LineAwesome', 'https://maxst.icons8.com/vue-static/landings/line-awesome/font-awesome-line-awesome/css/all.min.css', false, false );
	wp_enqueue_style( 'LineAwesome' );

	wp_register_style( 'lineAwesomeIcons', 'https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css', false, '1.3.0' );
	wp_enqueue_style( 'lineAwesomeIcons' );

}
add_action( 'wp_enqueue_scripts', 'IconsStyle' );