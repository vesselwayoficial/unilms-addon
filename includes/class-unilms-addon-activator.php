<?php

/**
 * Fired during plugin activation
 *
 * @link       uesleinascimento.me
 * @since      1.0.0
 *
 * @package    Unilms_Addon
 * @subpackage Unilms_Addon/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Unilms_Addon
 * @subpackage Unilms_Addon/includes
 * @author     Ueslei Nascimento <uesleiconceic@gmail.com>
 */
class Unilms_Addon_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
